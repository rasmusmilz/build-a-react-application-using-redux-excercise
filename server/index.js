var express = require('express');
var app = express();


app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/person/:id', function (req, res) {
  res.json({
    val1:1,
    val2:2
  });
})

app.get('/facility/1', function (req, res) {
  res.json({
    "val3" : 3,
    "val4" : 4
   });
})

app.get('/exposure/2', function (req, res) {
  res.json({
    "val5" : 5
   });
})

var server = app.listen(8081, function () {
  console.log("Server listening at 8081")
})
