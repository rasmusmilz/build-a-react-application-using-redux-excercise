import React, { Component, PropTypes } from 'react';


var styles = {
  overlay: {
    width:'100%',
    height: '100%',
    position: 'absolute',
    top: '0',
    left: '0',
    backgroundColor: 'grey',
    opacity: '0.6',
    zIndex: '10'
  },
  boxContainer:{
    position: 'absolute',
    top: '50%',
    left: '50%',
    zIndex: '11',
    backgroundColor: '#fff',
    width: '550px',
    height: '200px',
    marginTop: '-165px',
    marginLeft: '-329px',
    padding: '20px 30px'
  }
}


export default class extends Component {
  static propTypes = {
    text: PropTypes.string.isRequired
  }
  render() {
    const { text } = this.props;
    return(
      <div>
        <div style={styles.overlay}></div>
        <div style={styles.boxContainer}>
          Thank you for looking at this nice and pure awesome project, <br />
          here is the value of val3 multiplied by val4:
          <br />
          <br />
          <br />
          {text}
        </div>
      </div>
    )
  }
}
