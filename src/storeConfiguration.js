import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './rootReducer'


export function configureStoreDev(initStore) {
  const loggerMiddleware = createLogger();

  const createStoreWithMiddleware = compose(
      applyMiddleware(
          thunkMiddleware, // lets us dispatch() functions
          loggerMiddleware // neat middleware that logs actions
        ),
      window.devToolsExtension ? window.devToolsExtension() : f => f
    )(createStore);
  const store = createStoreWithMiddleware(rootReducer, initStore);
  return store
}
