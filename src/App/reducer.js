import update from 'react/lib/update'
import * as actionConsts from './actions'

export const initState = {
  val: "",
  isFetching: false,
  values: null
}

export default function App(state=initState, action) {
  switch (action.type) {
    case actionConsts.UPDATE_INPUT_VAL:
      return update(state, {
        val: {
          $set: action.val
        }
      });
    case actionConsts.GET_VALUES_REQUEST:
      return update(state, {
        isFetching: {
          $set: true
        }
      });
    case actionConsts.GET_VALUES_SUCCESS:
      return update(state, {
        isFetching: {
          $set: false
        },
        values: {
          $set: action.values
        }
      });
    case actionConsts.GET_VALUES_FAILURE:
      return update(state, {
        isFetching: {
          $set: false
        }
      });
    default:
      return state;
  }
}
