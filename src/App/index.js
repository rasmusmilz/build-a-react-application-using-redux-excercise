import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import logo from '../../public/logo.svg';
import './App.css';
import Overlay from '../Common/overlay';
import {
  updateInputVal,
  attemptGetValues
} from './actions'

class App extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    val: PropTypes.string.isRequired,
    values: PropTypes.object,
    isFetching: PropTypes.bool.isRequired
  }
  isButtonDisabled = () => {
    const { val } = this.props;
    let regEx  = /[^a-z\d]/i;
    return regEx.test(val) || val.length < 1 || val.length > 10
  }
  render() {
    const { dispatch, val, values, isFetching } = this.props;
    return (
      <div className="App">

        {
          values ?
          <Overlay
            text={(values.val3*values.val4).toString()}
          />
          :
          null
        }
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Hello welcome to: Build a react application (using Redux)</h2>
        </div>
        <div>
          <input
            value={val}
            onChange={(evt) => dispatch(updateInputVal(evt.target.value))}
          />
          <button
            disabled={this.isButtonDisabled() || isFetching}
            onClick={() => dispatch(attemptGetValues())}>
              {!isFetching ?
                "Submit your value"
                :
                //add some spinner here to indicate that we are fetching something.
                <img src={logo} className="App-logo" alt="logo" />
              }
            </button>
        </div>
      </div>
    );
  }
}

const selectprops = state => {
  return {
    ...state.appReducer
  }
}

export default connect(selectprops)(App);
