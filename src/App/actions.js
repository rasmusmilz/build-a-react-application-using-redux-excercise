//SETTINGS:
let IS_DEV = true;

//endpoint url
let __URL__ = {
  webApiEndPoint: IS_DEV ? 'http://localhost:8081' : 'http://fubar.com'
}


export const UPDATE_INPUT_VAL = 'UPDATE_INPUT_VAL';
export const updateInputVal = (val) => (
    {
        type: UPDATE_INPUT_VAL,
        val
    }
)

export const GET_VALUES_REQUEST = 'GET_VALUES_REQUEST';
export const getValuesRequest = () => (
    {
        type: GET_VALUES_REQUEST
    }
)
export const GET_VALUES_SUCCESS = 'GET_VALUES_SUCCESS';
export const getValuesSuccess = (values) => (
    {
        type: GET_VALUES_SUCCESS,
        values
    }
)
export const GET_VALUES_FAILURE = 'GET_VALUES_FAILURE';
export const getValuesFailure = () => (
    {
        type: GET_VALUES_FAILURE
    }
)

export function attemptGetValues() {

    return (dispatch, getState) => {
    //first url
    let fetchUrl = __URL__.webApiEndPoint + '/person/' + getState().appReducer.val;
    dispatch(getValuesRequest());

    //container to store the values
    let values = {};

    return fetch(fetchUrl, {
        'headers': {
          'Accept': 'application/json'
        }
    })
    .then(response => unWrapResponseHelper(response))
    .then(json => {

      //update values container
      Object.assign(values, json);

      let fetchUrl = __URL__.webApiEndPoint + '/facility/' + values.val1;
      return fetch(fetchUrl, {
            'headers': {
              'Accept': 'application/json'
            }
      })
    })
    .then(response => unWrapResponseHelper(response))
    .then((json) => {

      //update values container
      Object.assign(values, json);

      let fetchUrl = __URL__.webApiEndPoint + '/exposure/' + values.val2;
      return fetch(fetchUrl, {
            'headers': {
              'Accept': 'application/json'
            }
      })
    })
    .then(response => unWrapResponseHelper(response))
    .then(json => {

      //update values container
      Object.assign(values, json);

      dispatch(getValuesSuccess(values));
    })
    .catch((e) => {
        console.log("something went wrong", e);
        dispatch(getValuesFailure());
    })
  };
}

function unWrapResponseHelper(response){
  if (response.status >= 400) {
      throw new Error("Bad response from server");
  }
  return response.json();
}
