import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './App/';
import './index.css';

import { configureStoreDev as configureStore} from './storeConfiguration'

const initialState = {}//Could be loaded from whereever

const store = configureStore(initialState);


ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
