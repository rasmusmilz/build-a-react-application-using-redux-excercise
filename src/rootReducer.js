import { combineReducers } from 'redux';
import appReducer from './App/reducer'

const rootReducer = combineReducers({
  appReducer
});

export default rootReducer;
