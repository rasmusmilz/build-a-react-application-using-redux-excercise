How to run this example:

This assumes that you have already installed node globally.

- App
In terminal
Type: npm i
Wait for all packages to be installed
Type: npm start
The app now runs on port 3000 on localhost


- Mock server
In terminal
Type: node server
The server now runs on port 8081 on localhost
